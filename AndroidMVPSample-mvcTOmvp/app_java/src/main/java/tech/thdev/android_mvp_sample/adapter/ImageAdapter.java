package tech.thdev.android_mvp_sample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

import tech.thdev.android_mvp_sample.adapter.contract.ImageAdapterContract;
import tech.thdev.android_mvp_sample.adapter.holder.ImageViewHolder;
import tech.thdev.android_mvp_sample.data.ImageItem;
import tech.thdev.android_mvp_sample.listener.OnImageItemClickListener;

/**
 * Created by tae-hwan on 10/23/16.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder> implements ImageAdapterContract.Model, ImageAdapterContract.View {

    private Context mContext;

    private ArrayList<ImageItem> mImageItems;
    private OnImageItemClickListener onImageItemClickListener;

    public ImageAdapter(Context context) {
        mContext = context;
    }

    public void setImageItems(ArrayList<ImageItem> imageItems) {
        mImageItems = imageItems;
    }

//    public void clear() {
//        if (mImageItems != null) {
//            mImageItems.clear();
//            mImageItems = null;
//        }
//    }

    //RecyclerView
    @Override
    public int getItemCount() {
        return mImageItems != null ? mImageItems.size() : 0;
    }

//    @Override
//    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new ImageViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_image, parent, false));
//    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(mContext, parent, onImageItemClickListener);
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, int position) {
        if (holder == null) return;

        holder.onBind(getItem(position), position);

//        new ImageAsync(holder.imageView).execute(imageItem.getImageRes());
    }

    //ImageAdapterContract.Model
    @Override
    public void addItem(ArrayList<ImageItem> imageItems) {
        mImageItems = imageItems;
    }

    @Override
    public void clearItem() {
        if(mImageItems != null) {
            mImageItems.clear();
        }
    }

    @Override
    public ImageItem getItem(int position) {
        return mImageItems.get(position);
    }

    //ImageAdapterContract.View
    @Override
    public void notifyAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public void setOnClickListener(OnImageItemClickListener onImageItemClickListener) {
        this.onImageItemClickListener = onImageItemClickListener;
    }

//    private class ImageAsync extends AsyncTask<Integer, Void, Bitmap> {
//
//        private final WeakReference<ImageView> imageViewReference;
//
//        ImageAsync(ImageView imageView) {
//            imageViewReference = new WeakReference<>(imageView);
//        }
//
//        @Override
//        protected Bitmap doInBackground(Integer... params) {
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize = 2;
//            return BitmapFactory.decodeResource(mContext.getResources(), params[0], options);
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            imageViewReference.get().setImageResource(0);
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap bitmap) {
//            super.onPostExecute(bitmap);
//            imageViewReference.get().setImageBitmap(bitmap);
//        }
//    }

//    public class ImageViewHolder extends RecyclerView.ViewHolder {
//
//        @BindView(R.id.img_view)
//        ImageView imageView;
//
//        public ImageViewHolder(View itemView) {
//            super(itemView);
//
//            ButterKnife.bind(this, itemView);
//        }
//    }
}
