package tech.thdev.android_mvp_sample.presenter;

import android.content.Context;

import java.util.ArrayList;

import tech.thdev.android_mvp_sample.adapter.contract.ImageAdapterContract;
import tech.thdev.android_mvp_sample.data.ImageItem;
import tech.thdev.android_mvp_sample.data.SampleImageData;
import tech.thdev.android_mvp_sample.listener.OnImageItemClickListener;

/**
 * Created by VSSCP_KYOSEON on 2017. 2. 15..
 */

public class MainPresenter implements MainContract.Presenter, OnImageItemClickListener {

	private MainContract.View view;

	private ImageAdapterContract.Model adapterModel;
	private ImageAdapterContract.View adapterView;

	private SampleImageData sampleImageData;

	@Override
	public void attachView(MainContract.View view) {
		this.view = view;
	}

	@Override
	public void detachView() {
		view = null;
	}

	@Override
	public void loadItems(Context context, boolean isClear) {
		ArrayList<ImageItem> items = sampleImageData.getImages(context, 10);
//		view.addItems(items, isClear);
//		view.notifyAdapter();

		if (isClear) {
			adapterModel.clearItem();
		}

		adapterModel.addItem(items);
		adapterView.notifyAdapter();
	}

	@Override
	public void setSampleImageData(SampleImageData sampleImageData) {
		this.sampleImageData = sampleImageData;
	}

	@Override
	public void setImageAdapterModel(ImageAdapterContract.Model adapterModel) {
 		this.adapterModel = adapterModel;
	}

	@Override
	public void setImageAdapterView(ImageAdapterContract.View adapterView) {
		this.adapterView = adapterView;

		adapterView.setOnClickListener(this);
	}

	@Override
	public void onItemClick(int position) {
		ImageItem imageItem = adapterModel.getItem(position);
		view.showToast(imageItem.getTitle());
	}
}
