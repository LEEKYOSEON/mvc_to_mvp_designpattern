package tech.thdev.android_mvp_sample.adapter.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import tech.thdev.android_mvp_sample.R;
import tech.thdev.android_mvp_sample.data.ImageAsync;
import tech.thdev.android_mvp_sample.data.ImageItem;
import tech.thdev.android_mvp_sample.listener.OnImageItemClickListener;

/**
 * Created by VSSCP_KYOSEON on 2017. 2. 15..
 */

public class ImageViewHolder extends RecyclerView.ViewHolder {
	private OnImageItemClickListener onImageItemClickListener;
	private Context mContext;
	@BindView(R.id.img_view)
	public ImageView imageView;

	public ImageViewHolder(Context mContext, ViewGroup parent, OnImageItemClickListener onImageItemClickListener) {
		super(LayoutInflater.from(mContext).inflate(R.layout.item_image, parent, false));

		this.mContext = mContext;
		this.onImageItemClickListener = onImageItemClickListener;

		ButterKnife.bind(this, itemView);

//		imageView.setOnClickListener(this);
	}

	public void onBind(ImageItem imageItem, final int position) {

		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onImageItemClickListener != null) {
					onImageItemClickListener.onItemClick(position);
				}
			}
		});

		new ImageAsync(imageView, mContext).execute(imageItem.getImageRes());
	}

//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//			case R.id.img_view:
////				Toast.makeText(this,"image touch",Toast.LENGTH_SHORT).show();
//				break;
//		}
//	}
}
