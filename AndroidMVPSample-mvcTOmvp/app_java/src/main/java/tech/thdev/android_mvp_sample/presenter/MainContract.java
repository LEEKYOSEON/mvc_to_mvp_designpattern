package tech.thdev.android_mvp_sample.presenter;

import android.content.Context;

import java.util.ArrayList;

import tech.thdev.android_mvp_sample.adapter.contract.ImageAdapterContract;
import tech.thdev.android_mvp_sample.data.ImageItem;
import tech.thdev.android_mvp_sample.data.SampleImageData;

/**
 * Created by VSSCP_KYOSEON on 2017. 2. 15..
 */

public interface MainContract {

	interface View {

//		void addItems(ArrayList<ImageItem> items, boolean isClear);
//
//		void notifyAdapter();

		void showToast(String title);
	}

	interface Presenter {
		void attachView(View view);

		void setImageAdapterModel(ImageAdapterContract.Model adapterModel);

		void setImageAdapterView(ImageAdapterContract.View adapterView);

		void detachView();

		void loadItems(Context context, boolean isClear);

		void setSampleImageData(SampleImageData sampleImageData);
	}
}
