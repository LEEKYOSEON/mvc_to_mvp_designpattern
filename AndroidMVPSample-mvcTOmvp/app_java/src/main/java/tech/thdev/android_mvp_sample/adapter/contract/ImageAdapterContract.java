package tech.thdev.android_mvp_sample.adapter.contract;

import java.util.ArrayList;

import tech.thdev.android_mvp_sample.data.ImageItem;
import tech.thdev.android_mvp_sample.listener.OnImageItemClickListener;

/**
 * Created by VSSCP_KYOSEON on 2017. 2. 15..
 */

public interface ImageAdapterContract {

	interface View {
		void notifyAdapter();

		void setOnClickListener(OnImageItemClickListener onImageItemClickListener);
	}

	interface Model {
		void addItem(ArrayList<ImageItem> imageItems);

		void clearItem();

		ImageItem getItem(int position);
	}
}
