package tech.thdev.android_mvp_sample.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by VSSCP_KYOSEON on 2017. 2. 16..
 */

public class ImageAsync extends AsyncTask<Integer, Void, Bitmap> {

	private WeakReference<ImageView> imageViewReference;
	private Context mContext;

	public ImageAsync(ImageView imageView, Context mContext) {
		imageViewReference = new WeakReference<>(imageView);
		this.mContext = mContext;
	}

	@Override
	protected Bitmap doInBackground(Integer... params) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		return BitmapFactory.decodeResource(mContext.getResources(), params[0], options);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		imageViewReference.get().setImageResource(0);
	}

	@Override
	protected void onPostExecute(Bitmap bitmap) {
		super.onPostExecute(bitmap);
		imageViewReference.get().setImageBitmap(bitmap);
	}
}
