package tech.thdev.android_mvp_sample.listener;

/**
 * Created by VSSCP_KYOSEON on 2017. 2. 16..
 */

public interface OnImageItemClickListener {

	void onItemClick(int position);
}
